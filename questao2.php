<?php  
function gera_numero($aQuantidade, $aMinimo, $aMaximo) {
   if( $aQuantidade > ($aMaximo - $aMinimo) ) {
       return false;
   }

   $numeros = range($aMinimo, $aMaximo);
   shuffle($numeros);
   array_splice($numeros, 0, $aQuantidade);
   sort($numeros);
   return $numeros;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <?php  
  echo "<b>Todos os números:</b> <br>";
  $numeros = gera_numero(8, 1, 25);
  var_dump(gera_numero(8, 1, 25));

  echo "<br>";
  echo "<br>";
  ?>
  <h2>Jogo 1</h2>
  <table class="table table-bordered">
    <tbody>
    <?php
    echo "<br><b>Números sorteados:</b> <br>";
	$numeros_sorteados = ["1", "4", "5", "6", "9", "10", "11", "12", "19", "20", "21", "22", "23", "24", "25"];

	var_dump($numeros_sorteados);

    for ($i=0; $i <= 4; $i++) { //linhas
    ?>
      <tr>
      <?php  
      $tamanho = sizeof($numeros) / 4;
      $count = 0;
      for ($j=0; $j <= $tamanho; $j++) { //colunas
      	//if ($j <= 24) {
      		if ($numeros[$j] == $numeros_sorteados[$j]) {
      ?>
        <td style="background-color: green;"><?php echo $numeros[$j]; ?></td>
      <?php
      		} else {
      ?>
      			<td><?php echo $numeros[$j]; ?></td>
      <?php
      		}
  		$count++;
      	//}
  	  }
      ?>
      </tr>
    <?php  
 	}
    ?>
    </tbody>
  </table>

  <h2>Jogo 2</h2>           
  <table class="table table-bordered">
    <tbody>
    <?php

    echo "<br><b>Números sorteados:</b> <br>";
	$numeros_sorteados = ["2", "3", "4", "5", "8", "9", "10", "11", "18", "19", "21", "22", "23", "24", "25"];

	var_dump($numeros_sorteados);

    for ($i=0; $i <= 4; $i++) { //linhas
    ?>
      <tr>
      <?php  
      $tamanho = sizeof($numeros) / 4;
      $count = 0;
      for ($j=0; $j <= $tamanho; $j++) { //colunas
      	//if ($j <= 24) {
      		if ($numeros[$j] == $numeros_sorteados[$j]) {
      ?>
        <td style="background-color: green;"><?php echo $numeros[$j]; ?></td>
      <?php
      		} else {
      ?>
      			<td><?php echo $numeros[$j]; ?></td>
      <?php
      		}
  		$count++;
      	//}
  	  }
      ?>
      </tr>
    <?php  
 	}
    ?>
    </tbody>
  </table>

  <h2>Jogo 3</h2>           
  <table class="table table-bordered">
    <tbody>
    <?php

    echo "<br><b>Números sorteados:</b> <br>";
	$numeros_sorteados = ["3", "5", "12", "13", "14", "15", "16", "17", "18", "19", "21", "22", "23", "24", "25"];

	var_dump($numeros_sorteados);

    for ($i=0; $i <= 4; $i++) { //linhas
    ?>
      <tr>
      <?php  
      $tamanho = sizeof($numeros) / 4;
      $count = 0;
      for ($j=0; $j <= $tamanho; $j++) { //colunas
      	//if ($j <= 24) {
      		if ($numeros[$j] == $numeros_sorteados[$j]) {
      ?>
        <td style="background-color: green;"><?php echo $numeros[$j]; ?></td>
      <?php
      		} else {
      ?>
      			<td><?php echo $numeros[$j]; ?></td>
      <?php
      		}
  		$count++;
      	//}
  	  }
      ?>
      </tr>
    <?php  
 	}
    ?>
    </tbody>
  </table>
</div>

</body>
</html>
